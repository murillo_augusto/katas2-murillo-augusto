// comece a criar a sua função add na linha abaixo
function add(n1, n2) {
  //n1 = parseInt(prompt('digite o primeiro número para soma'));
  //n2 = parseInt(prompt('digite o segundo número para soma'));
  return n1 + n2;
}

// descomente a linha seguinte para testar sua função
//console.assert(add(3, 5) === 8, 'A função add não está funcionando como esperado');


// comece a criar a sua função multiply na linha abaixo
function multiply(n1, n2) {
  let count = 0;
  for (let i = 1; i <= n2; i++) {
    count = add(count, n1);
  }
  return count;
}

// descomente a linha seguinte para testar sua função

//console.log(multiply(4, 6) === 24, ' a funcao nao funciona');


// comece a criar a sua função power na linha abaixo
function power(n1, n2) {
  let count = 1;
  for (let i = 1; i <= n2; i++) {
    count = multiply(count, n1);
    console.log(count);
  }
  return count;
}

// descomente a linha seguinte para testar sua função
//console.assert(power(3, 4) === 81, 'A função power não está funcionando como esperado');


// comece a criar a sua função factorial na linha abaixo
function factorial(n1) {
  let count = 1;
  for (let i = 1; i <= n1; i++) {
    count = multiply(count, i);
  }
  return count;
}

// descomente a linha seguinte para testar sua função
//console.assert(factorial(5) === 120, 'A função factorial não está funcionando como esperado');


/**
 * BONUS (aviso: o grau de dificuldade é bem maior !!!)
 */

//PRECISO DE AJUDA PARA FINALZIAR O FIBONACCI, N ENTENDI MT BEM!
// crie a função fibonacci
function fibonacci(n1) {
  let arr = [];
  arr[0] = 0;
  arr[1] = 1;
  for (let i = 2; i <= n1; i++) {
    arr[i] = arr[i - 2] + arr[i - 1];
    console.log(arr);
  }
}

// descomente a linha seguinte para testar sua função
//console.assert(fibonacci(8) === 13, 'A função fibonacci não está funcionando como esperado');
